require("dotenv").config();
const express = require("express");
const {
  verifyAdminToken,
  verifyEmployeeToken,
  verifyUserToken,
} = require("./middlewares/Authentication");
const MongoClient = require("mongodb").MongoClient;
const app = express();
var cors = require("cors");
const port = process.env.PORT || 3000;
var connectionString = process.env.DATABASE_URL;

const User = require("./routes/User");
const Appointment = require("./routes/Appointment");
const AppointmentDate = require("./routes/AppointmentDate");
const Service = require("./routes/Service");
const WorkingHours = require("./routes/WorkingHours");
const Preferences = require("./routes/Preferences");

MongoClient.connect(connectionString, {
  useUnifiedTopology: true,
})
  .then((client) => {
    console.log("Connected to Database");
    const db = client.db("beauty-salon");
    //middlewares
    app.use(express.json());
    app.use(cors());

    // routes
    //users
    app.post("/user/register", (request, response) =>
      User.register(request, response, db, "user")
    );
    app.post("/user/login", (request, response) =>
      User.login(request, response, db)
    );
    app.get("/user", (request, response) =>
      User.getAllUsers(request, response, db, "user")
    );
    app.delete("/user/:id", verifyUserToken, (request, response) =>
      User.deleteUser(request, response, db, "user")
    );
    app.put("/user/:id", verifyUserToken, (request, response) =>
      User.updateUser(request, response, db, "user")
    );
    app.get("/send-email-reset-password/:email", (request, response) =>
      User.getUserByEmail(request, response, db)
    );
    app.patch("/reset-password/:id", (request, response) =>
      User.updateUserPassword(request, response, db)
    );
    app.get("/user/search-appointment/:userId/:date", (request, response) =>
      User.searchUserAppointment(request, response, db)
    );

    //employee
    app.post("/employee/register", verifyEmployeeToken, (request, response) =>
      User.register(request, response, db, "employee")
    );
    app.get("/employee/:userId", (request, response) =>
      User.getAllUsers(request, response, db, "employee")
    );
    app.delete("/employee/:id", verifyAdminToken, (request, response) =>
      User.deleteUser(request, response, db, "employee")
    );
    app.put("/employee/:id", verifyEmployeeToken, (request, response) =>
      User.updateUser(request, response, db, "employee")
    );
    app.get(
      "/employee/search-appointment/:employeeId/:date",
      (request, response) =>
        User.searchEmployeeAppointment(request, response, db)
    );

    //admin
    app.post("/admin/register", (request, response) =>
      User.register(request, response, db, "admin")
    );

    //appointment
    app.post("/appointment", verifyUserToken, (request, response) =>
      Appointment.addAppointment(request, response, db)
    );
    app.get("/appointment", (request, response) =>
      Appointment.getAllAppointments(request, response, db)
    );
    app.delete("/appointment/:id", verifyUserToken, (request, response) =>
      Appointment.deleteAppointment(request, response, db)
    );
    app.get("/appointment/:id", verifyUserTxoken, (request, response) =>
      Appointment.getAppointmentById(request, response, db)
    );
    app.patch("/appointment/:id", verifyUserToken, (request, response) =>
      Appointment.updateAppointment(request, response, db)
    );
    app.get("/appointment/:id/appointmentDates", (request, response) =>
      AppointmentDate.getDatesOfAppointment(request, response, db)
    );
    app.post("/unavailableDateOfMonth", (request, response) =>
      Appointment.getUnavailableDatesOfMonth(request, response, db)
    );
    app.get("/remind-appointment/:userId", (request, response) =>
      AppointmentDate.getCloseAppointmentDates(request, response, db)
    );
    app.post("/available-slice-hour/", (request, response) =>
      WorkingHours.getWorkingHourSlicesAvailable(request, response, db)
    );

    // appointment date
    app.post("/appointmentDate", verifyUserToken, (request, response) =>
      AppointmentDate.addAppointmentDate(request, response, db)
    );
    app.get("/appointmentDate", (request, response) =>
      AppointmentDate.getAllAppointmentDates(request, response, db)
    );
    app.delete("/appointmentDate/:id", verifyUserToken, (request, response) =>
      AppointmentDate.deleteAppointmentDate(request, response, db)
    );
    app.get("/appointmentDate/:id", verifyUserToken, (request, response) =>
      AppointmentDate.getAppointmentDateById(request, response, db)
    );
    app.patch("/appointmentDate/:id", verifyUserToken, (request, response) =>
      AppointmentDate.updateAppointmentDate(request, response, db)
    );

    //services
    app.post("/service", verifyAdminToken, (request, response) =>
      Service.addService(request, response, db)
    );
    app.get("/available-service/:userId", (request, response) =>
      Service.getAllAvailableServices(request, response, db)
    );
    app.get("/default-service/:userId", (request, response) =>
      Service.getAllDefaultServices(request, response, db, "http")
    );
    app.get("/search-service/:searchString", (request, response) =>
      Service.searchService(request, response, db)
    );
    app.get("/service/:id", (request, response) =>
      Service.getServiceById(request, response, db)
    );
    app.delete("/service/:id", verifyAdminToken, (request, response) =>
      Service.deleteService(request, response, db)
    );
    app.put("/service/:id", verifyAdminToken, (request, response) =>
      Service.updateService(request, response, db)
    );
    app.get("/service/defaut", (request, response) =>
      Service.getAllServicesPerCategory(request, response, db, "default")
    );
    app.get("/service/special", (request, response) =>
      Service.getAllServicesPerCategory(request, response, db, "special offer")
    );
    app.get("/services", (request, response) =>
      Service.getAllRealServices(request, response, db)
    );
    app.get("/currentSpecialOffer", (request, response) =>
      Service.getCurrentSpecialOffer(request, response, db, "http")
    );

    //preferences
    app.post(
      "/user/add-employee-preference",
      verifyUserToken,
      (request, response) =>
        Preferences.addEmployeeToPreference(request, response, db)
    );
    app.post(
      "/user/add-service-preference",
      verifyUserToken,
      (request, response) =>
        Preferences.addServiceToPreference(request, response, db)
    );
    app.get(
      "/user/service-preference/:userId",
      verifyUserToken,
      (request, response) =>
        Preferences.getUserPreference(
          request,
          response,
          db,
          "userServicePreference"
        )
    );
    app.get(
      "/user/employee-preference/:userId",
      verifyUserToken,
      (request, response) =>
        Preferences.getUserPreference(
          request,
          response,
          db,
          "userEmployeePreference"
        )
    );
    app.delete(
      "/user/service-preference/:id/:userId",
      verifyUserToken,
      (request, response) =>
        Preferences.deleteUserPreference(
          request,
          response,
          db,
          "userServicePreference"
        )
    );
    app.delete(
      "/user/employee-preference/:id/:userId",
      verifyUserToken,
      (request, response) =>
        Preferences.deleteUserPreference(
          request,
          response,
          db,
          "userEmployeePreference"
        )
    );
  })
  .catch((error) => console.error("errorHERE" + error));

app.listen(port, function () {
  console.log("listening on 3000");
});
