# m1p10mean-loic-tsanta-backend

Data Model

User{
\_id
name
email
password
role (employee / admin / user)
workingHours[]
etat
}

Service{
\_id
name
duration (min)
price
commission %
type (default / special offer)
beginning
expiration
tasks
}

TaskFinished{
id
appointment
employeeId
serviceTask
}

Appointments{
\_id
User
employee
Service
status
}

AppointmentsDates{
\_id
Appointment
start
end
}
//user 65d892ff878048769020b431
//service 65d89d7e836c97640c31e3d7

UserEmployeePreferences{
\_id
User (employee)
User (user)
}

UserServicePreferences{
\_id
Service
User (user)
}

Depenses{
\_id
name
amount
date
}
