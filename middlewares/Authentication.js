var jwt = require("jsonwebtoken");
const PRIVATE_KEY = process.env.PRIVATE_KEY;

module.exports = {
  verifyAdminToken: function (req, res, next) {
    const token = req.header("authorization")?.split(" ")[1];
    if (token === undefined) {
      return res.send(401, {
        message: "Unauthorized",
      });
    } else {
      const decoded = jwt.verify(token, PRIVATE_KEY);
      if (decoded.role === "admin") {
        next();
      } else {
        res.status(401).send({
          message: "Unauthorized",
        });
      }
    }
  },
  verifyEmployeeToken: function (req, res, next) {
    const token = req.header("authorization")?.split(" ")[1];
    if (token === undefined) {
      return res.send(401, {
        message: "Unauthorized",
      });
    } else {
      const decoded = jwt.verify(token, PRIVATE_KEY);
      if (decoded.role === "employee") {
        next();
      } else {
        res.status(401).send({
          message: "Unauthorized",
        });
      }
    }
  },
  verifyUserToken: function (req, res, next) {
    const token = req.header("authorization")?.split(" ")[1];
    if (token === undefined) {
      res.status(401).send({
        message: "Unauthorized",
      });
    } else {
      const decoded = jwt.verify(token, PRIVATE_KEY);
      if (decoded.role === "user") {
        next();
      } else {
        res.status(401).send({
          message: "Unauthorized",
        });
      }
    }
  },
};
