const { ObjectId } = require("mongodb");
var jwt = require("jsonwebtoken");
var nodemailer = require("nodemailer");
const PRIVATE_KEY = process.env.PRIVATE_KEY;
function register(request, response, db, role) {
  if (request.body.role != role) {
    return response.status(500).json({
      message: "role of the user doesnt match the request",
    });
  }
  const usersCollection = db.collection("users");
  usersCollection
    .insertOne(request.body)
    .then((newUser) => {
      return response.status(201).json({
        id: newUser,
        role: role,
        token: jwt.sign({ id: newUser._id, role: role }, PRIVATE_KEY, {
          expiresIn: role === "user" ? "24h" : "10h",
        }),
      });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message:
          "error adding " +
          (role === "user"
            ? "user"
            : role === "employee"
            ? "employee"
            : "admin"),
      });
    });
}
function getAllUsers(request, response, db, role) {
  const usersCollection = db.collection("users");
  const userId = request.params.userId;
  let objectToMatch = {
    role: role,
  };
  if (role == "employee" && userId != "") {
    objectToMatch = {
      role: role,
      status: "create",
    };
    usersCollection
      .aggregate([
        {
          $match: {
            role: "employee",
            status: "create",
          },
        },
        {
          $lookup: {
            from: "userEmployeePreference",
            localField: "_id",
            foreignField: "employeeId",
            as: "employeePreference",
            pipeline: [
              {
                $match: {
                  userId: new ObjectId(userId),
                },
              },
            ],
          },
        },
        { $sort: { employeePreference: -1 } },
      ])
      .toArray()
      .then((results) => {
        return response.status(200).json(results);
      })
      .catch((error) => {
        console.error(error);
        return response.status(500).json({
          message: "error getting all users",
        });
      });
  } else {
    usersCollection
      .find(objectToMatch)
      .toArray()
      .then((results) => {
        return response.status(200).json(results);
      })
      .catch((error) => {
        console.error(error);
        return response.status(500).json({
          message: "error getting all users",
        });
      });
  }
}

function login(request, response, db) {
  const usersCollection = db.collection("users");
  usersCollection
    .find({
      email: request.body.email,
      password: request.body.password,
    })
    .toArray()
    .then((results) => {
      if (results.length === 1) {
        return response.status(200).json({
          user: results[0],
          token: jwt.sign(
            { id: results[0]._id, role: results[0].role },
            PRIVATE_KEY,
            { expiresIn: results[0].role === "user" ? "24h" : "10h" }
          ),
        });
      } else {
        return response.status(401).json({
          message: "email or password not valid",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error on login",
      });
    });
}

function deleteUser(request, response, db, role) {
  const usersCollection = db.collection("users");
  usersCollection
    .deleteOne({
      _id: new ObjectId(request.body._id),
      role: role,
    })
    .then((result) => {
      if (result.deletedCount === 0) {
        return response.status(404).json({
          message: "user to delete not found",
        });
      } else {
        return response.status(200).json({
          message: "user deleted",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error deleting user",
      });
    });
}

function updateUser(request, response, db, role) {
  const userId = request.params;
  const usersCollection = db.collection("users");
  usersCollection
    .replaceOne(
      {
        _id: new ObjectId(userId),
        role: role,
      },
      request.body
    )
    .then((result) => {
      if (result.modifiedCount === 0) {
        return response.status(404).json({
          message: "user to update not found",
        });
      } else {
        return response.status(200).json({
          message: "user updated",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error updating user",
      });
    });
}
function updateUserPassword(request, response, db) {
  const userId = request.params.id;
  const usersCollection = db.collection("users");
  usersCollection
    .updateOne(
      { _id: new ObjectId(userId) },
      { $set: { password: request.body.password } }
    )
    .then((result) => {
      if (result.modifiedCount === 0) {
        return response.status(404).json({
          message: "user to update password not found",
        });
      } else {
        return response.status(200).json({
          message: "user password updated",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error updating user password",
      });
    });
}

function getUserByEmail(request, response, db) {
  const email = request.params.email;
  const usersCollection = db.collection("users");
  usersCollection
    .find({
      email: email,
    })
    .toArray()
    .then((results) => {
      if (results.length > 0) {
        var transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: process.env.EMAIL,
            pass: process.env.MDP,
          },
        });
        var link =
          process.env.FRONTEND_URL + "/reset-password/" + results[0]._id;
        var mailOptions = {
          from: "loyk.raj@gmail.com",
          to: email,
          subject: "Beauty salon reset password",
          html:
            "<h1>Use that link to reset your password : </h1><a href=" +
            link +
            ">Reset Password</a>",
        };
        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            console.log(error);
            return response.status(404).json({
              message: "email not founded",
            });
          } else {
            return response.status(200).json({
              message: "email sended",
            });
          }
        });
      } else {
        return response.status(404).json({
          message: "email not founded",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error getting all users",
      });
    });
}
async function getEmployeeById(db, employeeId) {
  const usersCollection = db.collection("users");
  let result = null;
  await usersCollection
    .find({
      role: "employee",
      _id: new ObjectId(employeeId),
    })
    .toArray()
    .then((results) => {
      if (results.length > 0) {
        result = results[0];
      }
    })
    .catch((error) => {
      console.error(error);
    });
  return result;
}
async function getWorkingHours(db, dateToCheck, employeeId) {
  dateToCheck = new Date(dateToCheck);
  const employee = await getEmployeeById(db, employeeId);
  let targetWorkingHours;
  let dateDebutOfWorkingHours;
  let dateDiff;
  let currentDateDiff;
  if (employee != null) {
    for (let i = 0; i < employee.workingHours.length; i++) {
      dateDebutOfWorkingHours = new Date(employee.workingHours[i].date);
      currentDateDiff =
        dateToCheck.getTime() - dateDebutOfWorkingHours.getTime();
      if (dateDiff == undefined) {
        dateDiff = currentDateDiff;
        targetWorkingHours = employee.workingHours[i];
        continue;
      }
      if (currentDateDiff > 0 && currentDateDiff < dateDiff) {
        dateDiff = currentDateDiff;
        targetWorkingHours = employee.workingHours[i];
      }
    }
  }
  return targetWorkingHours;
}
async function searchUserAppointment(request, response, db) {
  const date = request.params.date;
  const userId = new ObjectId(request.params.userId);
  const appointmentCollection = db.collection("appointmentDates");
  let result = null;
  await appointmentCollection
    .aggregate([
      {
        $addFields: {
          dateTime: {
            $dateFromString: {
              dateString: "$start",
            },
          },
        },
      },
      {
        $addFields: {
          date: {
            $dateToString: {
              format: "%Y-%m-%d",
              date: "$dateTime",
            },
          },
        },
      },
      { $match: { date: { $eq: date } } },
      {
        $lookup: {
          from: "appointments",
          localField: "appointment",
          foreignField: "_id",
          as: "appointmentDetails",
        },
      },
      {
        $match: {
          "appointmentDetails.0.user": new ObjectId(userId),
        },
      },
      {
        $lookup: {
          from: "services",
          localField: "appointmentDetails.0.service",
          foreignField: "_id",
          as: "serviceDetails",
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "appointmentDetails.0.employee",
          foreignField: "_id",
          as: "employeeDetails",
        },
      },
    ])
    .toArray()
    .then((results) => {
      if (results.length > 0) {
        result = results;
      }
    })
    .catch((error) => {
      console.error(error);
    });
  if (result == null) {
    return response.status(404).json({
      message: "no appointment founded",
    });
  }
  let formattedResults = [];
  for (let i = 0; i < result.length; i++) {
    formattedResults.push({
      _id: result[i]._id,
      service: result[i].serviceDetails[0].name,
      start: result[i].start,
      end: result[i].end,
      employee: result[i].employeeDetails[0].name,
    });
  }
  return response.status(200).json(formattedResults);
}
async function searchEmployeeAppointment(request, response, db) {
  const date = request.params.date;
  const employeeId = new ObjectId(request.params.employeeId);
  const appointmentCollection = db.collection("appointmentDates");
  let result = null;
  await appointmentCollection
    .aggregate([
      {
        $addFields: {
          dateTime: {
            $dateFromString: {
              dateString: "$start",
            },
          },
        },
      },
      {
        $addFields: {
          date: {
            $dateToString: {
              format: "%Y-%m-%d",
              date: "$dateTime",
            },
          },
        },
      },
      { $match: { date: { $eq: date } } },
      {
        $lookup: {
          from: "appointments",
          localField: "appointment",
          foreignField: "_id",
          as: "appointmentDetails",
        },
      },
      {
        $match: {
          "appointmentDetails.0.employee": new ObjectId(employeeId),
        },
      },
      {
        $lookup: {
          from: "services",
          localField: "appointmentDetails.0.service",
          foreignField: "_id",
          as: "serviceDetails",
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "appointmentDetails.0.user",
          foreignField: "_id",
          as: "userDetails",
        },
      },
    ])
    .toArray()
    .then((results) => {
      if (results.length > 0) {
        result = results;
      }
    })
    .catch((error) => {
      console.error(error);
    });
  if (result == null) {
    return response.status(404).json({
      message: "no appointment founded",
    });
  }
  let formattedResults = [];
  for (let i = 0; i < result.length; i++) {
    formattedResults.push({
      _id: result[i]._id,
      service: result[i].serviceDetails[0].name,
      start: result[i].start,
      end: result[i].end,
      client: result[i].userDetails[0].name,
    });
  }
  return response.status(200).json(formattedResults);
}

exports.register = register;
exports.login = login;
exports.getAllUsers = getAllUsers;
exports.deleteUser = deleteUser;
exports.updateUser = updateUser;
exports.getUserByEmail = getUserByEmail;
exports.updateUserPassword = updateUserPassword;
exports.getWorkingHours = getWorkingHours;
exports.searchUserAppointment = searchUserAppointment;
exports.searchEmployeeAppointment = searchEmployeeAppointment;
