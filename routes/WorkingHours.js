const  User =require("./User");
const { ObjectId } = require("mongodb");
const  Service =require("./Service");
const  Appointment =require("./Appointment");


async function getWorkingHourSlicesAvailable(request,response,db){
    const employeeId=request.body.employeeId!=null?request.body.employeeId:null
    const date=request.body.date
    const serviceDuration=await Service.getServiceDurationById(request.body.serviceId,db)
    if(serviceDuration===false){
        return []
    }
    let listOfDates
    let takenHours=[]
    let workingHours
    let targetDate=new Date(date+"T00:00:00.000Z")
    let targetDatePlus1=new Date(date+"T00:00:00.000Z")
    targetDatePlus1.setHours(targetDatePlus1.getHours()+24)
    if(employeeId!=null){
        await db.collection("appointmentDates").aggregate(
            [
              {
                $addFields: {
                  startToDate: {
                    $dateFromString: {
                      dateString: '$start'
                    }
                  }
                }
              },
              {
                $addFields: {
                  isInsideDateSup: {
                    $and: [
                      {
                        $gte: [
                          '$startToDate',
                          targetDate
                        ]
                      }
                    ]
                  },
                  isInsideDateInf: {
                    $and: [
                      {
                        $lt: [
                          '$startToDate',
                          targetDatePlus1
                        ]
                      }
                    ]
                  }
                }
              },
              {
                $match: {
                  isInsideDateSup: true,
                  isInsideDateInf: true
                }
              }
            ],
            { maxTimeMS: 60000, allowDiskUse: true }
          )
        .toArray().then((results) => {
                listOfDates=results
            })
            .catch((error) => {
                console.error(error)
                return response.status(500).json({
                     message:"error getting all unavailable date on month",
                });
            });

            for (let z = 0; z < listOfDates.length; z++) {
                    takenHours.push({
                        startDate:Appointment.getDateRepresentationWithoutTZ(listOfDates[z].start),
                        endDate:Appointment.getDateRepresentationWithoutTZ(listOfDates[z].end)
                    })
            }
            workingHours =await User.getWorkingHours(db,targetDate,employeeId)
            
            let availableSliceHour=getAllAvailableSliceHour(workingHours,takenHours,serviceDuration)
            return response.status(200).json(
                availableSliceHour
            );
    }
}

function getAllAvailableSliceHour(workingHours,takenHours,serviceDuration){
    let listAvailableSliceHour=[]
    let takenHoursInWorkingHourSlice=[]
    workingHours=[
        {
            start:workingHours.startMorning+":00",
            end:workingHours.endMorning+":00"
        },
        {
            start:workingHours.startAfternoon+":00",
            end:workingHours.endAfternoon+":00"
        },
    ]
    if(takenHours.length===0){
        return workingHours
    }
    for (let i = 0; i < workingHours.length; i++) {
        takenHoursInWorkingHourSlice=[]
        for (let y = 0; y < takenHours.length; y++) {
            if(Appointment.isAHourInsideAnother(workingHours[i],takenHours[y].startDate,takenHours[y].endDate)){
                takenHoursInWorkingHourSlice.push({
                    startDate:takenHours[y].startDate,
                    endDate:takenHours[y].endDate,
                })
            }
        }
        if(takenHoursInWorkingHourSlice.length===0){
            let maxFreeMinutes=Appointment.getMinutesDifference(workingHours[i].start,workingHours[i].end)
            if(maxFreeMinutes>=serviceDuration){
                listAvailableSliceHour.push({
                    start:workingHours[i].start,
                    end:workingHours[i].end
                })
            }
        }
        else{
            takenHoursInWorkingHourSlice.sort((a,b)=>b.startDate.getTime()-a.startDate.getTime())
            let availableHour=getAllAvailableHourInSliceHour(workingHours[i],takenHoursInWorkingHourSlice,serviceDuration)
            if(availableHour.length>0){
                for (let y = 0; y < availableHour.length; y++) {
                    const element = availableHour[y];
                    listAvailableSliceHour.push(element)
                }
            }
        }
      
    }
    return listAvailableSliceHour
}

function getAllAvailableHourInSliceHour(workingHour,takenHoursOrdered,serviceDuration){
    let result=[]
    if(Appointment.getMinutesDifference(workingHour.start,Appointment.getTimeStringOfDate(takenHoursOrdered[0].startDate))>=serviceDuration){
        result.push({
            start:workingHour.start,
            end:Appointment.getTimeStringOfDate(takenHoursOrdered[0].startDate)
        })
    }
    if(Appointment.getMinutesDifference(Appointment.getTimeStringOfDate(takenHoursOrdered[takenHoursOrdered.length-1].endDate),workingHour.end)>=serviceDuration){
        result.push({
            start:Appointment.getTimeStringOfDate(takenHoursOrdered[takenHoursOrdered.length-1].endDate),
            end:workingHour.end
        })
    }
    for (let i = 0; i < takenHoursOrdered.length; i++) {
        if(i!=takenHoursOrdered.length-1){
            
            if( Appointment.getMinutesDifference(Appointment.getTimeStringOfDate(takenHoursOrdered[i].endDate),Appointment.getTimeStringOfDate(takenHoursOrdered[i+1].startDate))>=serviceDuration){
                result.push({
                    start:Appointment.getTimeStringOfDate(takenHoursOrdered[i].endDate),
                    end:Appointment.getTimeStringOfDate(takenHoursOrdered[i+1].startDate)
                })
            }
        }
    }
    return result
}
exports.getWorkingHourSlicesAvailable=getWorkingHourSlicesAvailable