const { ObjectId } = require("mongodb");
function isPreferenceAlreadyExist(db, collectionName, userId, preferenceId) {
  const preferenceCollection = db.collection(collectionName);
  let objectToMatch;
  if (collectionName == "userEmployeePreference") {
    objectToMatch = {
      userId: new ObjectId(userId),
      employeeId: new ObjectId(preferenceId),
    };
  } else {
    objectToMatch = {
      userId: new ObjectId(userId),
      serviceId: new ObjectId(preferenceId),
    };
  }
  preferenceCollection
    .find(objectToMatch)
    .toArray()
    .then((results) => {
      if (results.length > 0) {
        return true;
      }
      return false;
    })
    .catch((error) => {
      console.error(error);
      return false;
    });
}
function addEmployeeToPreference(request, response, db) {
  const userEmployeePreferenceCollection = db.collection(
    "userEmployeePreference"
  );
  if (
    !isPreferenceAlreadyExist(
      db,
      "userEmployeePreference",
      request.body.userId,
      request.body.employeeId
    )
  ) {
    request.body.userId = new ObjectId(request.body.userId);
    request.body.employeeId = new ObjectId(request.body.employeeId);
    userEmployeePreferenceCollection
      .insertOne(request.body)
      .then((newPreferencee) => {
        return response.status(201).json({
          id: newPreferencee,
        });
      })
      .catch((error) => {
        console.error(error);
        return response.status(500).json({
          message: "error adding new user employee preference",
        });
      });
  }
}
function addServiceToPreference(request, response, db) {
  const userServicePreferenceCollection = db.collection(
    "userServicePreference"
  );
  if (
    !isPreferenceAlreadyExist(
      db,
      "userServicePreference",
      request.body.userId,
      request.body.serviceId
    )
  ) {
    request.body.userId = new ObjectId(request.body.userId);
    request.body.serviceId = new ObjectId(request.body.serviceId);
    userServicePreferenceCollection
      .insertOne(request.body)
      .then((newPreferencee) => {
        return response.status(201).json({
          id: newPreferencee,
        });
      })
      .catch((error) => {
        console.error(error);
        return response.status(500).json({
          message: "error adding new user service preference",
        });
      });
  }
}

function getUserPreference(request, response, db, collectionName) {
  const userId = request.params.userId;
  const preferenceCollection = db.collection(collectionName);
  preferenceCollection
    .find({
      userId: new ObjectId(userId),
    })
    .toArray()
    .then((results) => {
      return response.status(200).json(results);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error getting user preferences",
      });
    });
}

function deleteUserPreference(request, response, db, collectionName) {
  const preferenceId = request.params.id;
  const userId = request.params.userId;
  const preferenceCollection = db.collection(collectionName);
  let objectToMatch;
  if (collectionName == "userEmployeePreference") {
    objectToMatch = {
      userId: new ObjectId(userId),
      employeeId: new ObjectId(preferenceId),
    };
  } else {
    objectToMatch = {
      userId: new ObjectId(userId),
      serviceId: new ObjectId(preferenceId),
    };
  }
  preferenceCollection
    .deleteOne(objectToMatch)
    .then((result) => {
      if (result.deletedCount === 0) {
        return response.status(404).json({
          message: "preference to delete not found",
        });
      } else {
        return response.status(200).json({
          message: "preference deleted",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: " Error deleting preference",
      });
    });
}

exports.addEmployeeToPreference = addEmployeeToPreference;
exports.addServiceToPreference = addServiceToPreference;
exports.getUserPreference = getUserPreference;
exports.deleteUserPreference = deleteUserPreference;
