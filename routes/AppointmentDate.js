const { ObjectId } = require("mongodb");

function addAppointmentDate(request,response,db){
    const appointmentDatesCollection = db.collection("appointmentDates");
    appointmentDatesCollection
    .insertMany(request.body)
    .then((newAppointmentDates) => {
        return response.status(201).json(newAppointmentDates);
    })
    .catch((error) => {
        console.error(error)
        return response.status(500).json({
            message:"error adding appointment date(s)",
        });
    });
}
async function insertManyAppointmentDate(db,dates){
    const appointmentDatesCollection = db.collection("appointmentDates");
    let success
    await appointmentDatesCollection
    .insertMany(dates)
    .then((newAppointmentDates) => {
        success= true
    })
    .catch((error) => {
        console.error(error)
        success= false
    });
    return success
}

function getAllAppointmentDates(request,response,db){
    const appointmentDatesCollection = db.collection("appointmentDates");
    appointmentDatesCollection
        .find()
        .toArray()
        .then((results) => {
            return response.status(200).json(results);
        })
        .catch((error) => {
            console.error(error)
            return response.status(500).json({
                message:"error getting all appointment dates",
            });
        });
}

function getAppointmentDateById(request,response,db){
    const appointmentDateId=request.params
    const appointmentDatesCollection = db.collection("appointmentDates");
    appointmentDatesCollection
        .find({
            "_id":new ObjectId(appointmentDateId),
        })
        .toArray()
        .then((results) => {
            if(results.length<=0){
                return response.status(404).json({
                    message:"appointment Date not found",
                });
            }else{
                return response.status(200).json(results);
            }
        })
        .catch((error) => {
            console.error(error)
            return response.status(500).json({
                message:"error getting all appointment dates",
            });
        });
}

function getDatesOfAppointment(request,response,db){
    const appointmentId=request.params.id
    const appointmentDatesCollection = db.collection("appointmentDates");
    appointmentDatesCollection
        .find({
            "idAppointment":appointmentId,
        })
        .toArray()
        .then((results) => {
            console.log(appointmentId)
            if(results.length<=0){
                return response.status(404).json({
                    message:"dates of the appointment are not found",
                });
            }else{
                return response.status(200).json(results);
            }
        })
        .catch((error) => {
            console.error(error)
            return response.status(500).json({
                message:"error getting all dates of the appointment ",
            });
        });
}

function deleteAppointmentDate(request,response,db){
    const appointmentDateId=request.params
    const appointmentDateCollection = db.collection("appointmentDates");
    appointmentDateCollection
    .deleteOne({
       "_id":new ObjectId(appointmentDateId),
    })
    .then((result) => {
        if(result.deletedCount===0){
            return response.status(404).json({
                message:"appointment date to delete not found",
            });
        }else{
            return response.status(200).json({
                message:"appointment date deleted",
            });
        }
    })
    .catch((error) => {
        console.error(error)
        return response.status(500).json({
            message:"error deleting appointmentDate",
        });
    });
}
function getCurrentDate(){
    let currentDateUTC= new Date();
    currentDateUTC.setHours(currentDateUTC.getHours()+3)
    return currentDateUTC
}

function updateAppointmentDate(request,response,db){
    const today=getCurrentDate()
    const appointmentDateId=request.params
    const appointmentDatesCollection = db.collection("appointmentDates");
    appointmentDatesCollection
    .replaceOne({
       "_id":new ObjectId(appointmentDateId),
    },
    request.body)
    .then((result) => {
        if(result.modifiedCount===0){
            return response.status(404).json({
                message:"appointment date to update not found",
            });
        }else{
            return response.status(200).json({
                message:"appointment date updated",
            });
        }
    })
    .catch((error) => {
        console.error(error)
        return response.status(500).json({
            message:"error updating appointment date",
        });
    });
}

function getCurrentDate(){
    let currentDateUTC= new Date();
    currentDateUTC.setHours(currentDateUTC.getHours()+3)
    return currentDateUTC
}
function getCloseAppointmentDates(request,response,db){
    const userId=request.params.userId
    const today=getCurrentDate()
    const appointmentDatesCollection = db.collection("appointmentDates");
    appointmentDatesCollection.aggregate(
        [
          {
            $addFields: {
              startDate: {
                $dateFromString: {
                  dateString: '$start'
                }
              },
              endDate: {
                $dateFromString: { dateString: '$end' }
              }
            }
          },
          {
            $lookup: {
              from: 'appointments',
              localField: 'appointment',
              foreignField: '_id',
              as: 'appointmentDetails'
            }
          },
          {
            $match: {
              'appointmentDetails.0.user':
              new ObjectId(userId)
            }
          },
          {
            $addFields: {
              beginningInfToday: {
                $lt: [
                    today,
                  '$startDate'
                ]
              },
              dateDistance: {
                $dateDiff: {
                  startDate: today,
                  endDate: '$startDate',
                  unit: 'day'
                }
              }
            }
          },
          {
            $match: {
              beginningInfToday: true,
              dateDistance: 1
            }
          },
          {
            $lookup: {
              from: 'services',
              localField:
                'appointmentDetails.0.service',
              foreignField: '_id',
              as: 'serviceDetails'
            }
          }
        ]
      )
        .toArray()
        .then((results) => {
            if(results.length<=0){
                return response.status(404).json({
                    message:"no close appointment dates found",
                });
            }else{
                return response.status(200).json(results);
            }
        })
        .catch((error) => {
            console.error(error)
            return response.status(500).json({
                message:"error getting close appointment dates",
            });
        });
}

exports.addAppointmentDate=addAppointmentDate
exports.insertManyAppointmentDate=insertManyAppointmentDate
exports.getAllAppointmentDates=getAllAppointmentDates
exports.deleteAppointmentDate=deleteAppointmentDate
exports.getAppointmentDateById=getAppointmentDateById
exports.updateAppointmentDate=updateAppointmentDate 
exports.getDatesOfAppointment=getDatesOfAppointment
exports.getCloseAppointmentDates=getCloseAppointmentDates