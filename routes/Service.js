const { ObjectId } = require("mongodb");

async function getAllDefaultServices(request, response, db, httpOrData) {
  const userId = request.params.userId;
  const servicesCollection = db.collection("services");
  let returnedData = null;
  await servicesCollection
    .aggregate([
      { $match: { type: "default" } },
      {
        $lookup: {
          from: "userServicePreference",
          localField: "_id",
          foreignField: "serviceId",
          as: "servicePreference",
          pipeline: [
            {
              $match: {
                userId: new ObjectId(userId),
              },
            },
          ],
        },
      },
      { $sort: { servicePreference: -1 } },
    ])
    .toArray()
    .then((results) => {
      if (httpOrData == "http") {
        return response.status(200).json(results);
      }
      returnedData = results;
    })
    .catch((error) => {
      if (httpOrData == "data") {
        return null;
      }
      return response.status(500).json({
        message: "error getting all default services",
      });
    });
  return returnedData;
}

function pushValueOfArrayInArray(arrayToIterate, targetArray) {
  for (let i = 0; i < arrayToIterate.length; i++) {
    targetArray.push(arrayToIterate[i]);
  }
}

async function getAllAvailableServices(request, response, db) {
  let finalArray = [];
  pushValueOfArrayInArray(
    await getAllDefaultServices(request, response, db, "data"),
    finalArray
  );
  pushValueOfArrayInArray(
    await getCurrentSpecialOffer(request, response, db, "data"),
    finalArray
  );
  const withError = finalArray.some((element) => element === null);
  if (finalArray.length > 0 && !withError) {
    return response.status(200).json(finalArray);
  } else if (withError) {
    return response.status(500).json({
      message: "error getting all available services",
    });
  }
  return response.status(404).json({
    message: "available services not found",
  });
}

async function searchService(request, response, db) {
  const searchString = request.params.searchString;
  let availableService = [];
  pushValueOfArrayInArray(
    await getCurrentSpecialOffer(request, response, db, "data"),
    availableService
  );
  pushValueOfArrayInArray(
    await getAllDefaultServices(request, response, db, "data"),
    availableService
  );
  const withError = availableService.some((element) => element === null);
  if (availableService.length > 0 && !withError) {
    let matchService = [];
    for (let i = 0; i < availableService.length; i++) {
      if (
        availableService[i].name
          .toLowerCase()
          .includes(searchString.toLowerCase())
      ) {
        matchService.push(availableService[i]);
      }
    }
    return response.status(200).json(matchService);
  } else if (withError) {
    return response.status(500).json({
      message: "error getting all available services",
    });
  }
  return response.status(404).json({
    message: "available services not found",
  });
}

function getServiceById(request, response, db) {
  const serviceId = request.params;
  const servicesCollection = db.collection("services");
  servicesCollection
    .find({
      _id: new ObjectId(serviceId),
    })
    .toArray()
    .then((results) => {
      if (results.length <= 0) {
        return response.status(404).json({
          message:
            "Backend : Service.js : getServiceById() : service not found",
        });
      } else {
        return response.status(200).json(results);
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message:
          "Backend : Service.js : getServiceById() : A little probleme with the backend",
      });
    });
}

async function getServiceDurationById(id, db) {
  if (id == "") {
    return false;
  }
  const servicesCollection = db.collection("services");
  let duration;
  await servicesCollection
    .find({
      _id: new ObjectId(id),
    })
    .toArray()
    .then((results) => {
      duration = results[0].duration;
    })
    .catch((error) => {
      return error;
    });
  return duration;
}

async function getServiceDataById(id, db) {
  if (id == "") {
    return false;
  }
  const servicesCollection = db.collection("services");
  let service;
  await servicesCollection
    .find({
      _id: new ObjectId(id),
    })
    .toArray()
    .then((results) => {
      service = results[0];
    })
    .catch((error) => {
      return error;
    });
  return service;
}

function addService(request, response, db) {
  const servicesCollection = db.collection("services");
  servicesCollection
    .insertOne(request.body)
    .then((newService) => {
      return response.status(201).json({
        id: newService,
      });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error adding new service",
      });
    });
}

function updateService(request, response, db) {
  const serviceId = request.params;
  const servicesCollection = db.collection("services");
  servicesCollection
    .replaceOne(
      {
        _id: new ObjectId(serviceId),
      },
      request.body
    )
    .then((result) => {
      if (result.modifiedCount === 0) {
        return response.status(404).json({
          message:
            "Backend : Service.js : updateService() : Service to update not found",
        });
      } else {
        return response.status(200).json({
          message: "Backend : Service.js : updateService() : Service updated",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message:
          "Backend : Service.js : updateService() : Error updating service",
      });
    });
}

function deleteService(request, response, db) {
  const serviceId = request.params;
  const serviceCollection = db.collection("services");
  serviceCollection
    .deleteOne({
      _id: new ObjectId(serviceId),
    })
    .then((result) => {
      if (result.deletedCount === 0) {
        return response.status(404).json({
          message:
            "Backend : Service.js : deleteService() : Service to delete not found",
        });
      } else {
        return response.status(200).json({
          message: "Backend : Service.js : deleteService() : Service deleted",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message:
          "Backend : Service.js : deleteService() : Error deleting service",
      });
    });
}
exports.deleteService = deleteService;

function getAllServicesPerCategory(request, response, db, category) {
  const servicesCollection = db.collection("services");
  servicesCollection
    .find({
      type: category,
    })
    .toArray()
    .then((results) => {
      return response.status(200).json(results);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error getting all default services",
      });
    });
}

function getAllRealServices(request, response, db) {
  const servicesCollection = db.collection("services");
  servicesCollection
    .find()
    .toArray()
    .then((results) => {
      return response.status(200).json(results);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message:
          "Backend : Service.js : deleteService() : Error where i'm getting all default services",
      });
    });
}

function getCurrentDate() {
  let currentDateUTC = new Date();
  currentDateUTC.setHours(currentDateUTC.getHours() + 3);
  return currentDateUTC;
}
async function getCurrentSpecialOffer(request, response, db, httpOrData) {
  const today = getCurrentDate();
  let returnedData = null;
  const servicesCollection = db.collection("services");
  await servicesCollection
    .aggregate([
      {
        $addFields: {
          beginningDate: {
            $dateFromString: {
              dateString: "$beginning",
            },
          },
          expirationDate: {
            $dateFromString: {
              dateString: "$expiration",
            },
          },
        },
      },
      { $match: { beginningDate: { $ne: null } } },
      {
        $addFields: {
          supToBeginnig: {
            $lte: ["$beginningDate", today],
          },
          infToExpiration: {
            $gte: ["$expirationDate", today],
          },
        },
      },
      {
        $match: {
          supToBeginnig: true,
          infToExpiration: true,
        },
      },
    ])
    .toArray()
    .then((results) => {
      if (httpOrData === "http") {
        return response.status(200).json(results);
      }
      returnedData = results;
    })
    .catch((error) => {
      console.error(error);
      if (httpOrData === "http") {
        return response.status(500).json({
          message: "Error getting special offers",
        });
      }
    });
  return returnedData;
}

exports.getAllRealServices = getAllRealServices;
exports.getAllDefaultServices = getAllDefaultServices;
exports.addService = addService;
exports.updateService = updateService;
exports.getServiceById = getServiceById;
exports.getCurrentSpecialOffer = getCurrentSpecialOffer;
exports.deleteService = deleteService;
exports.getServiceDurationById = getServiceDurationById;
exports.getServiceDataById = getServiceDataById;
exports.getAllAvailableServices = getAllAvailableServices;
exports.searchService = searchService;
exports.getAllServicesPerCategory = getAllServicesPerCategory;
