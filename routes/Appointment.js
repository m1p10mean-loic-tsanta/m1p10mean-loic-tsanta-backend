const { ObjectId } = require("mongodb");
const Service = require("./Service");
const User = require("./User");
const AppointmentDate = require("./AppointmentDate");

async function addAppointment(request, response, db) {
  const appointmentsCollection = db.collection("appointments");
  let appointmentId;
  request.body.user = new ObjectId(request.body.user);
  request.body.service = new ObjectId(request.body.service);
  request.body.employee = new ObjectId(request.body.user);
  await appointmentsCollection
    .insertOne(request.body.appointment)
    .then((newAppointment) => {
      appointmentId = newAppointment;
      for (let i = 0; i < request.body.dates.length; i++) {
        request.body.dates[i].appointment = newAppointment.insertedId;
      }
    })
    .catch((error) => {
      console.error(error);
    });
  if (appointmentId != undefined) {
    let addingAppointmentSuccess =
      await AppointmentDate.insertManyAppointmentDate(db, request.body.dates);
    if (addingAppointmentSuccess) {
      return response.status(200).json({
        message: "appointment added successfully",
      });
    }
  }
  return response.status(500).json({
    message: "error adding appointment",
  });
}

function getAllAppointments(request, response, db) {
  const appointmentsCollection = db.collection("appointments");
  appointmentsCollection
    .find()
    .toArray()
    .then((results) => {
      return response.status(200).json(results);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error getting all appointments",
      });
    });
}

function getAppointmentById(request, response, db) {
  const appointmentId = request.params;
  const appointmentsCollection = db.collection("appointments");
  appointmentsCollection
    .find({
      _id: new ObjectId(appointmentId),
    })
    .toArray()
    .then((results) => {
      if (results.length <= 0) {
        return response.status(404).json({
          message: "appointment not found",
        });
      } else {
        return response.status(200).json(results);
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error getting all appointments",
      });
    });
}

function deleteAppointment(request, response, db) {
  const appointmentId = request.params;
  const appointmentCollection = db.collection("appointments");
  appointmentCollection
    .deleteOne({
      _id: new ObjectId(appointmentId),
    })
    .then((result) => {
      if (result.deletedCount === 0) {
        return response.status(404).json({
          message: "appointment to delete not found",
        });
      } else {
        return response.status(200).json({
          message: "appointment deleted",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error deleting appointment",
      });
    });
}

function updateAppointment(request, response, db) {
  const appointmentId = request.params;
  const appointmentsCollection = db.collection("appointments");
  appointmentsCollection
    .updateOne(
      {
        _id: new ObjectId(appointmentId),
      },
      { $set: request.body }
    )
    .then((result) => {
      if (result.modifiedCount === 0) {
        return response.status(404).json({
          message: "appointment to update not found",
        });
      } else {
        return response.status(200).json({
          message: "appointment updated",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error updating appointment",
      });
    });
}

async function getUnavailableDatesOfMonth(request, response, db) {
  const month = request.body.month;
  const year = request.body.year;
  const employeeId =
    request.body.employeeId != null ? request.body.employeeId : null;
  const serviceDuration = await Service.getServiceDurationById(
    request.body.serviceId,
    db
  );
  if (serviceDuration === false) {
    return [];
  }
  const appointmentDateCollection = db.collection("appointmentDates");
  let listOfDates;
  if (employeeId != null) {
    await appointmentDateCollection
      .aggregate([
        {
          $addFields: {
            month: { $month: { $toDate: "$start" } },
            year: { $year: { $toDate: "$start" } },
            date: {
              $dateToString: {
                format: "%Y-%m-%d",
                date: { $toDate: "$start" },
              },
            },
            duration: {
              $dateDiff: {
                startDate: { $toDate: "$start" },
                endDate: { $toDate: "$end" },
                unit: "minute",
              },
            },
          },
        },
        { $match: { month: month, year: year } },
        {
          $lookup: {
            from: "appointments",
            localField: "appointment",
            foreignField: "_id",
            as: "currentAppointment",
          },
        },
        {
          $match: {
            "currentAppointment.0.employee": new ObjectId(employeeId),
          },
        },
      ])
      .toArray()
      .then((results) => {
        listOfDates = results;
      })
      .catch((error) => {
        console.error(error);
        return response.status(500).json({
          message: "error getting all unavailable date on month",
        });
      });
    const unavailableDates = await getUnavailableDates(
      serviceDuration,
      listOfDates,
      db,
      employeeId
    );
    return response.status(200).json(unavailableDates);
  }
}
async function getUnavailableDates(serviceDuration, listDates, db, employeeId) {
  let unavailableDates = [];
  let concernedDates = [];
  let maxFreeWorkMinutesInARow;
  let takenHours = [];
  let workingHours;
  for (let i = 0; i < listDates.length; i++) {
    if (!concernedDates.includes(listDates[i].date)) {
      concernedDates.push(listDates[i].date);
    }
  }
  for (let y = 0; y < concernedDates.length; y++) {
    takenHours = [];
    for (let z = 0; z < listDates.length; z++) {
      if (listDates[z].date == concernedDates[y]) {
        takenHours.push({
          startDate: getDateRepresentationWithoutTZ(listDates[z].start),
          endDate: getDateRepresentationWithoutTZ(listDates[z].end),
        });
      }
    }
    workingHours = await User.getWorkingHours(
      db,
      concernedDates[y],
      employeeId
    );
    maxFreeWorkMinutesInARow = getMaxWorkingHours(workingHours, takenHours);

    if (maxFreeWorkMinutesInARow < serviceDuration) {
      unavailableDates.push(concernedDates[y]);
    }
  }
  return unavailableDates;
}

function getMaxWorkingHours(workingHours, takenHours) {
  let maxFreeWorkMinutesInARow = 0;
  let takenHoursInWorkingHourSlice = [];
  workingHours = [
    {
      start: workingHours.startMorning + ":00",
      end: workingHours.endMorning + ":00",
    },
    {
      start: workingHours.startAfternoon + ":00",
      end: workingHours.endAfternoon + ":00",
    },
  ];
  for (let i = 0; i < workingHours.length; i++) {
    takenHoursInWorkingHourSlice = [];
    for (let y = 0; y < takenHours.length; y++) {
      if (
        isAHourInsideAnother(
          workingHours[i],
          takenHours[y].startDate,
          takenHours[y].endDate
        )
      ) {
        takenHoursInWorkingHourSlice.push({
          startDate: takenHours[y].startDate,
          endDate: takenHours[y].endDate,
        });
      }
    }
    if (takenHoursInWorkingHourSlice.length === 0) {
      let maxFreeMinutes = getMinutesDifference(
        workingHours[i].start,
        workingHours[i].end
      );
      if (maxFreeMinutes > maxFreeWorkMinutesInARow) {
        maxFreeWorkMinutesInARow = maxFreeMinutes;
      }
    } else {
      takenHoursInWorkingHourSlice.sort(
        (a, b) => b.startDate.getTime() - a.startDate.getTime()
      );
      let maxFreeMinutes = getMaxFreeWorkMinutesInARowInSliceHour(
        workingHours[i],
        takenHoursInWorkingHourSlice
      );
      if (maxFreeMinutes > maxFreeWorkMinutesInARow) {
        maxFreeWorkMinutesInARow = maxFreeMinutes;
      }
    }
  }
  return maxFreeWorkMinutesInARow;
}
function getMaxFreeWorkMinutesInARowInSliceHour(
  workingHour,
  takenHoursOrdered
) {
  let freeMinutesInARow = 0;
  freeMinutesInARow = getMinutesDifference(
    workingHour.start,
    getTimeStringOfDate(takenHoursOrdered[0].startDate)
  );
  let freeMinutesInARowAtEndOfSliceHour = getMinutesDifference(
    getTimeStringOfDate(
      takenHoursOrdered[takenHoursOrdered.length - 1].endDate
    ),
    workingHour.end
  );
  if (freeMinutesInARowAtEndOfSliceHour > freeMinutesInARow) {
    freeMinutesInARow = freeMinutesInARowAtEndOfSliceHour;
  }
  for (let i = 0; i < takenHoursOrdered.length; i++) {
    if (i != takenHoursOrdered.length - 1) {
      let currentfreeMinutesInARow = getMinutesDifference(
        getTimeStringOfDate(takenHoursOrdered[i].endDate),
        getTimeStringOfDate(takenHoursOrdered[i + 1].startDate)
      );
      if (currentfreeMinutesInARow > freeMinutesInARow) {
        freeMinutesInARow = currentfreeMinutesInARow;
      }
    }
  }
  return freeMinutesInARow;
}
function getDateRepresentationWithoutTZ(stringDate) {
  const parts = stringDate.split(/[-T:.]+/);
  const year = parseInt(parts[0]);
  const monthIndex = parseInt(parts[1]) - 1;
  const day = parseInt(parts[2]);
  const hours = parseInt(parts[3]);
  const minutes = parseInt(parts[4]);
  const seconds = parseInt(parts[5]);
  return new Date(Date.UTC(year, monthIndex, day, hours, minutes, seconds));
}
function getTimeStringOfDate(date) {
  let hourString = (date.getUTCHours() < 10 ? "0" : "") + date.getUTCHours();
  let minuteString =
    (date.getUTCMinutes() < 10 ? "0" : "") + date.getUTCMinutes();
  let secondString =
    (date.getUTCSeconds() < 10 ? "0" : "") + date.getUTCSeconds();
  return hourString + ":" + minuteString + ":" + secondString;
}
function getMinutesDifference(start, end) {
  const [hours1, minutes1, seconds1] = end.split(":");
  const [hours2, minutes2, seconds2] = start.split(":");
  const totalMinutes1 =
    parseInt(hours1) * 60 + parseInt(minutes1) + parseInt(seconds1) / 60;
  const totalMinutes2 =
    parseInt(hours2) * 60 + parseInt(minutes2) + parseInt(seconds2) / 60;

  const differenceInMinutes = Math.abs(totalMinutes1 - totalMinutes2);

  return differenceInMinutes;
}
function isAHourInsideAnother(workingHours, startDate, endDate) {
  let startHourOk = false;
  let endHourOk = false;

  if (compareDateToDateString(startDate, workingHours.start, "start")) {
    startHourOk = true;
  }
  if (compareDateToDateString(endDate, workingHours.end, "end")) {
    endHourOk = true;
  }
  return startHourOk && endHourOk ? true : false;
}
function compareDateToDateString(date, dateString, endOrStart) {
  if (endOrStart == "start") {
    if (date.getUTCHours() > parseInt(dateString.split(":")[0])) {
      return true;
    } else if (date.getUTCHours() == parseInt(dateString.split(":")[0])) {
      if (date.getUTCMinutes() >= parseInt(dateString.split(":")[1])) {
        return true;
      } else if (date.getUTCMinutes() == parseInt(dateString.split(":")[1])) {
        if (date.getUTCSeconds() >= parseInt(dateString.split(":")[2])) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  } else {
    if (date.getUTCHours() < parseInt(dateString.split(":")[0])) {
      return true;
    } else if (date.getUTCHours() == parseInt(dateString.split(":")[0])) {
      if (date.getUTCMinutes() <= parseInt(dateString.split(":")[1])) {
        return true;
      } else if (date.getUTCMinutes() == parseInt(dateString.split(":")[1])) {
        if (date.getUTCSeconds() <= parseInt(dateString.split(":")[2])) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
async function getUsersWorkingHoursById(id, db) {
  if (id == "") {
    return false;
  }
  const usersCollection = db.collection("users");
  let response;
  await usersCollection
    .find({
      _id: new ObjectId(id),
    })
    .toArray()
    .then((results) => {
      response = results[0].workingHours;
    })
    .catch((error) => {
      return null;
    });
  return response;
}

exports.addAppointment = addAppointment;
exports.getAllAppointments = getAllAppointments;
exports.deleteAppointment = deleteAppointment;
exports.getAppointmentById = getAppointmentById;
exports.updateAppointment = updateAppointment;
exports.getUnavailableDatesOfMonth = getUnavailableDatesOfMonth;
exports.getDateRepresentationWithoutTZ = getDateRepresentationWithoutTZ;
exports.getTimeStringOfDate = getTimeStringOfDate;
exports.getMinutesDifference = getMinutesDifference;
exports.isAHourInsideAnother = isAHourInsideAnother;
