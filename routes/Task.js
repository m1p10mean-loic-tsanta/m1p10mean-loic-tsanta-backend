const { ObjectId } = require("mongodb");
function isTaskFinishedAlreadyExist(db, employeeId, appointmentId) {
  const preferenceCollection = db.collection("TaskFinished");
  preferenceCollection
    .find({
      employeeId: new ObjectId(employeeId),
      appointmentId: new ObjectId(appointmentId),
    })
    .toArray()
    .then((results) => {
      if (results.length > 0) {
        return true;
      }
      return false;
    })
    .catch((error) => {
      console.error(error);
      return false;
    });
}
function addTaskToTaskFinished(request, response, db) {
  const taskFinishedCollection = db.collection("taskFinished");
  if (
    !isTaskFinishedAlreadyExist(
      db,
      request.body.employeeId,
      request.body.appointmentId
    )
  ) {
    request.body.userId = new ObjectId(request.body.userId);
    request.body.employeeId = new ObjectId(request.body.employeeId);
    taskFinishedCollection
      .insertOne(request.body)
      .then((newTaskFinished) => {
        return response.status(201).json({
          id: newTaskFinished,
        });
      })
      .catch((error) => {
        console.error(error);
        return response.status(500).json({
          message: "error adding new task finished",
        });
      });
  }
}

function getTaskFinished(request, response, db) {
  const employeeId = request.params.employeeId;
  const appointmentId = request.params.appointmentId;
  const taskFinishedCollection = db.collection("taskFinished");
  taskFinishedCollection
    .find({
      employeeId: new ObjectId(employeeId),
      appointmentId: new ObjectId(appointmentId),
    })
    .toArray()
    .then((results) => {
      return response.status(200).json(results);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: "error getting task finished",
      });
    });
}

function deleteTaskFinished(request, response, db) {
  const appointmentId = request.params.appointmentId;
  const employeeId = request.params.employeeId;
  const taskFinishedCollection = db.collection("taskFinished");

  taskFinishedCollection
    .deleteOne({
      employeeId: new ObjectId(employeeId),
      appointmentId: new ObjectId(appointmentId),
    })
    .then((result) => {
      if (result.deletedCount === 0) {
        return response.status(404).json({
          message: "task finished to delete not found",
        });
      } else {
        return response.status(200).json({
          message: "task finished deleted",
        });
      }
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({
        message: " Error deleting task finished",
      });
    });
}

exports.addTaskToTaskFinished = addTaskToTaskFinished;
exports.getTaskFinished = getTaskFinished;
exports.deleteTaskFinished = deleteTaskFinished;
